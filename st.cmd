require essioc
require rfcond

# General
epicsEnvSet("M", "DTL-010:SC-FSM-002")
epicsEnvSet("EVR", "DTL-010:RFS-EVR-101:")
epicsEnvSet("LLRF", "DTL-010:RFS-LLRF-101:")
epicsEnvSet("LLRFD", "DTL-010:RFS-DIG-101:")
epicsEnvSet("FIM", "DTL-010:RFS-FIM-101:")
epicsEnvSet("RFS", "DTL-010:RFS-")
#- VACX configuration for DTL
epicsEnvSet("VAC1","DTL-010:Vac-VGC-10000:PrsR CPP")
epicsEnvSet("VAC2","DTL-010:Vac-VGC-50000:PrsR CPP")
epicsEnvSet("VAC3","")
epicsEnvSet("VAC4","")

#- VACIX are same for DTL1 and RFQ
epicsEnvSet("VACI1","DTL-010:RFS-VacMon-110:Status-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI2","DTL-010:RFS-VacMon-120:Status-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI3","DTL-010:RFS-VacMon-130:Status-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI4","DTL-010:RFS-VacMon-140:Status-Ilck-RB.RVAL CPP")

#Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")


iocshLoad("$(rfcond_DIR)/rfcond.iocsh")

iocInit()
date                                                                                                                                           
